import { Component, DestroyRef, inject, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { VisualizationData } from './model/visualization-data';
import { KypoAdaptiveTransitionVisualizationPollingService } from './services/kypo-adaptive-transition-visualization-polling.service';
import { KypoAdaptiveTransitionVisualizationService } from './services/kypo-adaptive-transition-visualization.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'kypo-adaptive-transition-visualization',
  templateUrl: 'kypo-adaptive-transition-visualization.component.html',
  styleUrls: ['kypo-adaptive-transition-visualization.component.scss'],
})
export class KypoAdaptiveTransitionVisualizationComponent implements OnInit {
  @Input() trainingInstanceId!: number;
  @Input() trainingRunId!: number;
  @Input() progress!: boolean;
  @Input() transitionData?: VisualizationData;

  data$!: Observable<VisualizationData>;
  hasError$!: Observable<boolean>;
  isLoading$!: Observable<boolean>;

  destroyRef = inject(DestroyRef);

  constructor(
    private visualizationPollingService: KypoAdaptiveTransitionVisualizationPollingService,
    private visualizationService: KypoAdaptiveTransitionVisualizationService,
  ) {}

  ngOnInit() {
    this.init();
  }

  private init(): void {
    if (this.transitionData) {
      this.data$ = of(this.transitionData);
    } else {
      if (this.trainingInstanceId && this.progress) {
        this.initPollingServiceForTrainingInstance();
      }

      if (this.trainingInstanceId && !this.progress) {
        this.initServiceForTrainingInstance();
      }

      if (this.trainingRunId) {
        this.initServiceForTrainingRun();
      }
    }
  }

  initPollingServiceForTrainingInstance() {
    this.data$ = this.visualizationPollingService.visualizationData$;
    this.visualizationPollingService
      .getAll(this.trainingInstanceId)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe();
  }

  initServiceForTrainingInstance() {
    this.data$ = this.visualizationService.visualizationData$;
    this.visualizationService
      .getAllForTrainingInstance(this.trainingInstanceId)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe();
  }

  initServiceForTrainingRun() {
    this.data$ = this.visualizationService.visualizationData$;
    this.visualizationService
      .getAllForTrainingRun(this.trainingRunId)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe();
  }
}
