import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { VisualizationData } from '../model/visualization-data';
import { HttpErrorResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { KypoAdaptiveTransitionVisualizationApi } from '../api/kypo-adaptive-transition-visualization-api.service';

@Injectable({
  providedIn: 'root',
})
export class KypoAdaptiveTransitionVisualizationService {
  /**
   * True if server returned error response on the latest request, false otherwise
   * Change internally in extending service. Client should subscribe to the observable
   */
  private hasErrorSubject$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  /**
   * True if server returned error response on the latest request, false otherwise
   * @contract must be updated every time new data are received
   */
  hasError$: Observable<boolean> = this.hasErrorSubject$.asObservable();

  /**
   * True if response to the latest request was not yet received
   * Change internally in extending service. Client should subscribe to the observable
   */
  private isLoadingSubject$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  /**
   * True if response to the latest request was not yet received
   * @contract must be updated every time new data are received
   */
  isLoading$: Observable<boolean> = this.isLoadingSubject$.asObservable();

  private visualizationDataSubject$: BehaviorSubject<VisualizationData> = new BehaviorSubject<VisualizationData>(
    null as unknown as VisualizationData,
  );

  visualizationData$!: Observable<VisualizationData>;

  constructor(private visualizationApi: KypoAdaptiveTransitionVisualizationApi) {
    this.visualizationData$ = this.visualizationDataSubject$.asObservable();
  }

  /**
   * Gets all visualization data and updates related observables or handles an error
   * @param trainingInstanceId id of training instance
   */
  getAllForTrainingInstance(trainingInstanceId: number): Observable<VisualizationData> {
    return this.callForTrainingInstance(trainingInstanceId).pipe(
      tap(
        (data) => {
          this.isLoadingSubject$.next(false);
          this.visualizationDataSubject$.next(data);
        },
        (err) => this.onGetAllError(err),
      ),
    );
  }

  /**
   * Gets all stages and updates related observables or handles an error
   * @param trainingRunId id of training run
   */
  getAllForTrainingRun(trainingRunId: number): Observable<VisualizationData> {
    return this.callForTrainingRun(trainingRunId).pipe(
      tap(
        (data) => {
          this.isLoadingSubject$.next(false);
          this.visualizationDataSubject$.next(data);
        },
        (err) => this.onGetAllError(err),
      ),
    );
  }

  protected callForTrainingInstance(trainingRunId: number): Observable<VisualizationData> {
    return this.visualizationApi.getDataForTrainingInstance(trainingRunId);
  }

  protected callForTrainingRun(trainingRund: number): Observable<VisualizationData> {
    return this.visualizationApi.getDataForTrainingRun(trainingRund);
  }

  protected onGetAllError(err: HttpErrorResponse): void {
    this.hasErrorSubject$.next(true);
  }
}
