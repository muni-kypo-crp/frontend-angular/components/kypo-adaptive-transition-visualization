import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisualizationRoutingModule } from './visualization-routing.module';
import { VisualizationComponent } from './visualization.component';
import { KypoAdaptiveTransitionVisualizationModule } from '@muni-kypo-crp/adaptive-transition-visualization';
import { CustomConfig } from '../../custom-config';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [VisualizationComponent],
  imports: [
    CommonModule,
    VisualizationRoutingModule,
    KypoAdaptiveTransitionVisualizationModule.forRoot(CustomConfig),
    MatCardModule,
  ],
  exports: [VisualizationComponent],
})
export class VisualizationModule {}
